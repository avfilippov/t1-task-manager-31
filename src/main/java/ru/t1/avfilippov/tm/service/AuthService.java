package ru.t1.avfilippov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.service.IAuthService;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.api.service.IUserService;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.exception.field.LoginEmptyException;
import ru.t1.avfilippov.tm.exception.field.PasswordEmptyException;
import ru.t1.avfilippov.tm.exception.user.AccessDeniedException;
import ru.t1.avfilippov.tm.exception.user.PermissionException;
import ru.t1.avfilippov.tm.model.User;
import ru.t1.avfilippov.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private final IPropertyService propertyService;

    private String userId;

    public AuthService(final IUserService userService, final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @Nullable final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        @NotNull final String hash = HashUtil.salt(propertyService, password);
        @NotNull final Boolean locked = user.getLocked() == null || user.getLocked();
        if (locked) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Nullable
    @Override
    public String getUserId() {
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

}
