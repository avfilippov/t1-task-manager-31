package ru.t1.avfilippov.tm.exception.user;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email is already exists...");
    }

}
