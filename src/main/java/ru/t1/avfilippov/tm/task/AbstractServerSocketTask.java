package ru.t1.avfilippov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    public AbstractServerSocketTask(
            @NotNull Server server,
            @NotNull Socket socket
    ) {
        super(server);
        this.socket = socket;
    }

}
